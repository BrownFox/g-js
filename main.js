(function() {
    
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');

    var canvasWidth = window.innerWidth;
    var canvasHeight = window.innerHeight;

    var DISTANCE_BETWEEN_VECTORS = 20;
    var PARTICLE_RADIUS = 5;

    var particles = [];

    function addMasses() {
        particles = [];
        particles.push( new Particle(500, 300, 100) );
        particles.push( new Particle(900, 500, 1000) );
        particles.push( new Particle(400, 600, 100) );
    }

    function Particle(x, y, mass) {
        this.x = x;
        this.y = y;
        this.mass = mass;
    }

    function Vector(r, theta) {
        this.r = r;
        this.theta = theta;
        this.x = r * Math.cos(theta);
        this.y = r * Math.sin(theta);

        this.updatePolar = function(r, theta) {
            this.r = r;
            this.theta = theta;
            this.x = r * Math.cos(theta);
            this.y = r * Math.sin(theta);
        };
        this.updateCartesian = function(x, y) {
            this.x = x;
            this.y = y;
            this.r = Math.sqrt(x*x+y*y);
            this.theta = 0;
            if(x < 0) {
                this.theta = Math.atan(y/x) + Math.PI;
            }else{
                this.theta = Math.atan(y/x);
            }
        };
    }

    function init() {
        window.addEventListener('resize', onResizeWindow, false);
        onResizeWindow();
    }

    function onResizeWindow() {
        canvasWidth = window.innerWidth;
        canvasHeight = window.innerHeight;
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
        addMasses();
        redraw();
    }

    function colorInterp(val) {
        if(val > 1) return "#FF0000"
        var r = 255*val;
        var g = 255*(1-val);
        var b = 0;
        return "#" + ("0" + Math.floor(r).toString(16)).slice(-2) + ("0" + Math.floor(g).toString(16)).slice(-2) + ("0" + Math.floor(b).toString(16)).slice(-2);
    }

    function redraw() {
        context.clearRect(0, 0, canvasWidth, canvasHeight);

        for(var i = 0; i < particles.length; i++) {
            context.fillStyle = "#000000";
            context.beginPath();
            context.arc(particles[i].x, particles[i].y, PARTICLE_RADIUS, 0, 2*Math.PI, false);
            context.fill();
        }

        for(var x = DISTANCE_BETWEEN_VECTORS; x <= canvasWidth; x += DISTANCE_BETWEEN_VECTORS) {
            for(var y = DISTANCE_BETWEEN_VECTORS; y <= canvasWidth; y += DISTANCE_BETWEEN_VECTORS) {

                var netForce = new Vector(0, 0);

                for(var i = 0; i < particles.length; i++) {

                    var force = new Vector(0, 0);

                    force.updateCartesian(particles[i].x-x, particles[i].y-y);

                    var distance = force.r;

                    force.updatePolar((particles[i].mass/distance)/distance, force.theta);

                    netForce.updateCartesian(netForce.x + force.x, netForce.y + force.y);
                }

                drawArrow(x, y, DISTANCE_BETWEEN_VECTORS/2, netForce.theta, colorInterp(netForce.r*10));
            }
        }
    }

    function drawArrow(cx, cy, length, direction, color) {
        //WARNING WIERD SIGNS ON Y DUE TO WAY CANVAS WORKS ( the fact that (0,0) is the top left
        var px = length/2 * Math.cos(direction);
        var py = length/2 * Math.sin(direction);
        context.strokeStyle = color;
        context.beginPath();
        context.lineWidth = 2;
        context.moveTo(cx-px, cy-py);
        context.lineTo(cx+px, cy+py);
        context.stroke();
        context.fillRect(cx+px-1, cy+py-1, 2, 2);
    }

    init();

})();